package repository

import (
	"crud-clean-architecture/model"
	"gorm.io/gorm"
)

type UserRepository interface {
	Save(user *model.User) (*model.User, error)
	FindAll() ([]model.User, error)
	Find(user *model.User) (*model.User, error)
}

type UserRepo struct {
	Db *gorm.DB
}

// Save ...
func (u *UserRepo) Save(user *model.User) (*model.User, error) {
	err := u.Db.Create(user).Error
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (u *UserRepo) FindAll() ([]model.User, error) {
	var users []model.User
	err := u.Db.Find(&users).Error
	if err != nil {
		return nil, err
	}
	return users, nil
}

func (u *UserRepo) Find(user *model.User) (*model.User, error) {
	err := u.Db.First(user).Error
	if err != nil {
		return nil, err
	}
	return user, nil
}
