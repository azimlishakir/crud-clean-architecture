package model

type User struct {
	Id       int64  `json:"id"`
	Name     string `json:"name"`
	LastName string `json:"lastName"`
	UserName string `json:"userName"`
	Password string `json:"password"`
	Age      int    `json:"age"`
}
