package controller

import (
	"crud-clean-architecture/config"
	handler "crud-clean-architecture/errors"
	"crud-clean-architecture/model"
	"crud-clean-architecture/service"
	"encoding/json"
	"net/http"
)

type UserController struct {
	userService service.UserService
}

func NewUserController(userService service.UserService) *UserController {
	return &UserController{userService}
}

func (controller *UserController) CreateUser(res http.ResponseWriter, req *http.Request) {
	res.Header().Set("Content-Type", "application/json")
	var user model.User
	json.NewDecoder(req.Body).Decode(&user)
	if err := config.DB.Where("name = ? AND last_name = ? "+
		"AND user_name =?", user.Name, user.LastName, user.UserName).First(&user).Error; err == nil {
		response := handler.ErrorResponse{Code: http.StatusBadRequest, Message: "user is exists"}
		json.NewEncoder(res).Encode(response)
		return
	}
	err := config.DB.Create(&user).Error

	if err != nil {
		response := handler.ErrorResponse{Code: http.StatusBadRequest, Message: err.Error()}
		json.NewEncoder(res).Encode(response)
		return
	}

	res.WriteHeader(http.StatusCreated)
}
