package service

import (
	"crud-clean-architecture/model"
	"crud-clean-architecture/repository"
	"golang.org/x/crypto/bcrypt"
)

type IWalletService interface {
	CreateUser(user *model.User) (*model.User, error)
}

type UserService struct {
	userRepo *repository.UserRepo
}

func NewUserService(userRepo *repository.UserRepo) *UserService {
	return &UserService{
		userRepo: userRepo,
	}
}

func (u *UserService) CreateUser(user *model.User) (*model.User, error) {
	hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	user.Password = string(hashedPassword)
	return u.userRepo.Save(user)
}
