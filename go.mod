module crud-clean-architecture

go 1.13

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.4.0
	golang.org/x/crypto v0.0.0-20220214200702-86341886e292
	gorm.io/driver/postgres v1.3.1
	gorm.io/gorm v1.23.2
)
