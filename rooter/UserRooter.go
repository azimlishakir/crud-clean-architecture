package rooter

import (
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
)

type userRooter struct{}

var (
	muxRouterInstance = mux.NewRouter()
)

func NewMuxRouter() Router {
	return &userRooter{}
}

func (*userRooter) POST(uri string, f func(w http.ResponseWriter, r *http.Request)) {
	muxRouterInstance.HandleFunc(uri, f).Methods("POST")
}

func (*userRooter) SERVE(port string) {
	fmt.Printf("Mux HTTP server running on port %v", port)
	http.ListenAndServe(port, muxRouterInstance)
}
