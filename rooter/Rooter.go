package rooter

import "net/http"

type Router interface {
	POST(uri string, f func(http.ResponseWriter, *http.Request))
	SERVE(port string)
}
