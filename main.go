package main

import (
	"crud-clean-architecture/config"
	"crud-clean-architecture/controller"
	"crud-clean-architecture/repository"
	"crud-clean-architecture/rooter"
	"crud-clean-architecture/service"
)

var (
	httpRouter     rooter.Router = rooter.NewMuxRouter()
	userRepository repository.UserRepository
)

func createUserController() *controller.UserController {
	userRepo := repository.UserRepo{}
	userService := service.NewUserService(&userRepo)
	userController := controller.NewUserController(*userService)
	return userController
}

func main() {
	const port string = ":8000"
	config.InitialMigration()
	//httpRouter.POST("/posts", postController.AddPost)

	httpRouter.POST("/users", createUserController().CreateUser)
	httpRouter.SERVE(port)
}
